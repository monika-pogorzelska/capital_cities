#!/usr/bin/env ruby
require_relative 'countries.rb'

answers = ['a', 'b', 'c', 'd']

players = []

score = []

def player_interaction(country, cities)
  puts "What is the capital of #{country}?"
  puts "a. #{cities[0][1]}"
  puts "b. #{cities[1][1]}"
  puts "c. #{cities[2][1]}"
  puts "d. #{cities[3][1]}"
  puts 'enter your answer:'
  STDIN.gets.chomp
end

def spaces(length)
  ' ' * (length / 2 - 4)
end

def bar(style, length)
  bar = style * (length - 2)
  puts '*' + bar + '*'
end

def ornament_head(char, length = 40)
  puts spaces(length) + '__-o8o-__'
  bar(char, length)
end

def ornament_tail(char, length = 40)
  bar(char, length)
  puts spaces(length) + '~~=====~~'
  puts
end

if ARGV.empty?
  num_of_players = 1
  num_of_rounds = 3
else
  num_of_players = ARGV[0].to_i
  num_of_rounds = ARGV[1].to_i
end

num_of_players.times do |count|
  puts "enter player #{count+1} name"
  players[count] = STDIN.gets.chomp
  score[count] = 0
end

question = countries.clone
round = 0
while round < num_of_rounds
  x = 0
  while x < num_of_players
    points = 0
    ornament_head('=')
    puts "question for player #{players[x]}"
    rand_num = rand(question.size)
    country = question[rand_num][0]
    correct = question[rand_num]
    answer = countries.clone
    answer.reject! do |item|
      item[0] == question[rand_num][0]
    end
    cities = answer.sample(3)
    cities[3] = correct
    cities.shuffle!
    i = cities.index(correct)
    correct_answer = answers[i]
    player = player_interaction(country, cities)
    if player != 'q'
      if player == correct_answer
        puts 'good answer!'
        points += 1
      else
        puts 'nope!'
      end
      question.delete_at(rand_num)
      score[x] += points
    else
      puts'player left the game'
    end
    x += 1
  end
  round += 1
end

ornament_tail('=')

place = 0
score.max.downto(0) do |number_of_points|
  points = score.each_index.select { |i| score[i] == number_of_points }
  if points.any?
    place += 1
    points.each do |item|
      puts "#{place} place :"
      puts "#{players[item]} got #{score[item]} points "
    end
  end
end
